package com.adrian.SpringFileServer.Model;

public class RequestLogin
{
	public String login;
	public String password;
	
	public String getLogin()
	{
		return login;
	}
	
	public void setLogin(String login)
	{
		this.login = login;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password)
	{
		this.password = password;
	}

	@Override
	public String toString() {
		return "RequestLogin [login=" + login + ", password=" + password + "]";
	}
	
	
}
