package com.adrian.SpringFileServer.Model;

public class DeleteFileResponse
{
	private boolean success;
	private String errMessage;
	
	public DeleteFileResponse( boolean b, String e )
	{
		success = b;
		errMessage = e;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}
	
	
}
