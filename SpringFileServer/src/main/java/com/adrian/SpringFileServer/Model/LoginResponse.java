package com.adrian.SpringFileServer.Model;

public class LoginResponse
{
	private boolean success;
	private String token;
	private String username;
	
	public LoginResponse( boolean b, String t, String u )
	{
		success = b;
		token = t;
		username = u;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
