package com.adrian.SpringFileServer.Model;

public class RequestDeleteFile
{

	private long fileID;
	private String userToken;
	
	public long getFileID(){
		return fileID;
	}
	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	public String getUserToken() {
		return userToken;
	}
	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
	
	
}
