package com.adrian.SpringFileServer.Model;

public class UploadDataResponse
{
	private String res;

	public UploadDataResponse(String res)
	{
		this.res = res;
	}

	public String getRes() {
		return res;
	}

	public void setRes(String res) {
		this.res = res;
	}
	
	
}
