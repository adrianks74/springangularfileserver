package com.adrian.SpringFileServer.Model;

public class NewAccountResponse
{
	private boolean created;
	private String errMessage;
	
	public NewAccountResponse( boolean b, String e )
	{
		created = b;
		errMessage = e;
	}

	public boolean isCreated() {
		return created;
	}

	public void setCreated(boolean created) {
		this.created = created;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}
	
	
}
