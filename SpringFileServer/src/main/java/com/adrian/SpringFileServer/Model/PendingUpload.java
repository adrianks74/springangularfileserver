package com.adrian.SpringFileServer.Model;

public class PendingUpload
{
	private int uploadID;
	private Long userID;
	private Long fileSizeBytes;
	private String fileName;
	private String extension;
	
	
	public int getUploadID() {
		return uploadID;
	}
	public void setUploadID(int uploadID) {
		this.uploadID = uploadID;
	}
	public Long getUserID() {
		return userID;
	}
	public void setUserID(Long userID) {
		this.userID = userID;
	}
	public Long getFileSizeBytes() {
		return fileSizeBytes;
	}
	public void setFileSizeBytes(Long fileSizeBytes) {
		this.fileSizeBytes = fileSizeBytes;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	
	
}
