package com.adrian.SpringFileServer.Model;

public class FileResponse
{
	private String name;
	
	private long sizeBytes;
	
	private long fileID;
	
	public FileResponse( FileDescriptor fDesc )
	{
		name = fDesc.getName()+fDesc.getExtension();
		sizeBytes = fDesc.getSizeBytes();
		fileID = fDesc.getId();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSizeBytes() {
		return sizeBytes;
	}

	public void setSizeBytes(long sizeBytes) {
		this.sizeBytes = sizeBytes;
	}

	public long getFileID() {
		return fileID;
	}

	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	
	
	
	
}
