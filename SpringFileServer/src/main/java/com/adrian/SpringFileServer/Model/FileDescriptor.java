package com.adrian.SpringFileServer.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "file")
public class FileDescriptor
{
	// IDS
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Long userId;

	// DESCRIPTOR
	private String directory;
	
	private String name;
	
	private String extension;
	
	private long sizeBytes;

	public FileDescriptor SetAll( long user, String dir, String fil, String ex, Long size )
	{
		userId = user;
		directory = dir;
		name = fil;
		extension = ex;
		sizeBytes = size;
		
		return this;
	}
	
	// Getters Setters
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getExtension()
	{
		return extension;
	}

	public void setExtension(String extension)
	{
		this.extension = extension;
	}

	public long getSizeBytes()
	{
		return sizeBytes;
	}

	public void setSizeBytes(long sizeBytes)
	{
		this.sizeBytes = sizeBytes;
	}
	
	
}
