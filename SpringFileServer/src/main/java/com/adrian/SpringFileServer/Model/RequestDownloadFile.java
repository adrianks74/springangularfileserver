package com.adrian.SpringFileServer.Model;

public class RequestDownloadFile
{
	private long fileID;
	private String userToken;
	
	public RequestDownloadFile( long fID, String t)
	{
		fileID = fID;
		userToken = t;
	}
	
	public long getFileID(){
		return fileID;
	}
	public void setFileID(long fileID) {
		this.fileID = fileID;
	}
	public String getUserToken() {
		return userToken;
	}
	public void setUserToken(String userToken) {
		this.userToken = userToken;
	}
}
