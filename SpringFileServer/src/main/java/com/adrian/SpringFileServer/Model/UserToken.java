package com.adrian.SpringFileServer.Model;

public class UserToken
{
	private Long userID;
	private String token;
	
	public UserToken( Long id, String t )
	{
		userID = id;
		token = t;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
