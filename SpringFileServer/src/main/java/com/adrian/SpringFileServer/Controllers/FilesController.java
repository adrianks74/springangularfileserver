package com.adrian.SpringFileServer.Controllers;

import java.util.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

import com.adrian.SpringFileServer.Model.DeleteFileResponse;
import com.adrian.SpringFileServer.Model.FileDescriptor;
import com.adrian.SpringFileServer.Model.FileResponse;
import com.adrian.SpringFileServer.Model.RequestUpload;
import com.adrian.SpringFileServer.Model.PendingUpload;
import com.adrian.SpringFileServer.Model.RequestDeleteFile;
import com.adrian.SpringFileServer.Model.RequestDownloadFile;
import com.adrian.SpringFileServer.Repositories.FileDescriptorRepo;

public class FilesController
{
	// ----------------------------------------- Singleton --------------------------------------------------------- //
	private static FilesController instance;
		
	public static FilesController Instance()
	{
		if(instance == null) instance = new FilesController();
		return instance;
	}
	
	// ----------------------------------------- VARS --------------------------------------------------------- //
	
	private final String defaultDirectory = "D:\\SpringFileServerDir\\";
	
	private int uploadIDPool = 0;
	
	private ArrayList<PendingUpload> pendingUploads = new ArrayList<PendingUpload>();

	// ----------------------------------------- DISK METHODS --------------------------------------------------------- //
	
	// Write on file system
	public boolean WriteFile( byte[] bytes, String path )
	{
		try
		{
			FileOutputStream out = new FileOutputStream(path);
			out.write(bytes);
			out.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public boolean DeleteFileFromDisk( String path )
	{
		File file = new File(path);
		return file.delete();
	}
		
	
	// ----------------------------------------- ACCESS METHODS --------------------------------------------------------- //

	
	// Returns the pending upload description with the given ID
	public PendingUpload GetPendingUpload( int id )
	{
		for(PendingUpload pu :pendingUploads )
			if(pu.getUploadID() == id)
				return pu;
		
		return null;
	}
	
	// Received actual bytes for a file upload request
	public String OnReceivedUpload( FileDescriptorRepo fileRepo, byte[] bytes, int reqID )
	{
		// Test if bytes exist
		if(bytes == null) return "Bytes = null";
		
		// Test if upload request exists
		PendingUpload pUpload = GetPendingUpload(reqID);
		if(pUpload == null) return "Pending upload doesnt exist with id = "+reqID;
		
		pendingUploads.remove(pUpload);		// Delete pending upload

		// Test if size matches the request
		Long fileSizeBytes = pUpload.getFileSizeBytes();
		if( bytes.length != fileSizeBytes ) return "Sizes dont match "+bytes.length+" vs "+fileSizeBytes;
		
		// Proceed with saving the file
		String fileName = pUpload.getFileName();
		String extension = pUpload.getExtension();
		
		// Create descriptor
		FileDescriptor newDesc = new FileDescriptor();
		newDesc.SetAll(pUpload.getUserID(), defaultDirectory, fileName, extension, fileSizeBytes );
		fileRepo.save(newDesc);
		
		// Try saving it on real file system, cancel if it fails
		if(!WriteFile( bytes, defaultDirectory+"file"+newDesc.getId()+extension ))
		{
			// Delete descriptor
			fileRepo.delete(newDesc);
			
			// return error
			return "Failed to write file on server side.";
		}

		// Return "ok" (no error)
		return "ok";
	}
	
	// Get cur storage
	public Long GetCurStorage( FileDescriptorRepo fileRepo, Long userID )
	{
		Long l = (long) 0;
		
		List<FileResponse> files = GetFilesFor( fileRepo,  userID );
		
		for( FileResponse fr : files )
			l += fr.getSizeBytes();
		
		return l;
	}
	
	// Treats an upload request
	public int OnReceivedUploadRequest( FileDescriptorRepo fileRepo, RequestUpload req, Long userID, Long userMaxStorage )
	{
		Long sizeBytes = req.getFileSizeBytes();
		String fileName = req.getFileName();
		
		// TEST FOR ERRORS. TODO: treat this errors better than just returning -1
		if(sizeBytes == null) return -1;
		if(sizeBytes == 0 ) return -1;
		if(fileName == null ) return -1;
		if(fileName.length() <= 0) return -1;
		
		// Test storage capacity
		Long curStorage = GetCurStorage(fileRepo,userID);
		if( curStorage + sizeBytes > userMaxStorage )
		{
			System.out.println("Attempt to upload file that's too big. "+(curStorage + sizeBytes) +" / "+userMaxStorage);
			return -1;
		}
		
		// Retrieve extension
		String ext = "";
		
		int index = fileName.lastIndexOf('.');
	    if(index > 0)
	    {
	      ext = fileName.substring(index);
	      fileName = fileName.substring(0,index);
	      
	      System.out.println("FileName = "+fileName+", Ext = "+ext);
	    }
		
		// No errors, proceed with requesting upload
		PendingUpload newPendingUpload = new PendingUpload();
		newPendingUpload.setUserID(userID);
		newPendingUpload.setUploadID(uploadIDPool);
		newPendingUpload.setFileSizeBytes(sizeBytes);
		newPendingUpload.setFileName(fileName);
		newPendingUpload.setExtension(ext);
		
		pendingUploads.add(newPendingUpload);
		uploadIDPool++;
		
		System.out.println("New pending upload for user = "+userID+", bytes = "+sizeBytes+", created with id = "+newPendingUpload.getUploadID());
		
		return newPendingUpload.getUploadID();
	}
	
	// Return path for files already stored in disk
	String CurFilePath( FileDescriptor fDesc )
	{
		return fDesc.getDirectory()+"file"+fDesc.getId()+fDesc.getExtension();
	}
	

	public File DownloadFile( FileDescriptorRepo fileRepo, Long userID, RequestDownloadFile req  )
	{
		//////////////////////////////////////////////// Sorta Duplicated code from DeleteFile
		Long fileID = req.getFileID();
		
		if(fileID == null) return null;
		if(fileID < 0) return null;
		
		// Find file
		FileDescriptor fDesc = fileRepo.findByIdAndUserId(fileID, userID);
		if(fDesc == null) return null;
		
		// FIS
		File file = null;
		file = new File(CurFilePath(fDesc));
		
		return file;
	}
	
	public DeleteFileResponse DeleteFile( FileDescriptorRepo fileRepo, Long userID, RequestDeleteFile req )
	{
		Long fileID = req.getFileID();
		
		if(fileID == null) return new DeleteFileResponse(false,"err");
		if(fileID < 0) return new DeleteFileResponse(false,"err");
		
		// Find file
		FileDescriptor fDesc = fileRepo.findByIdAndUserId(fileID, userID);
		if(fDesc == null) return new DeleteFileResponse(false,"unauth");
		
		// Try delete from disk
		if(!DeleteFileFromDisk( CurFilePath(fDesc) ) )
		{
			System.out.println("-----Could not delete file-----");
			// TODO: file will exist in the disk, and not in the Database
		}
		
		// Delete from repository
		fileRepo.delete(fDesc);
		System.out.println("DELETED DECSRIPTOR");
		
		return new DeleteFileResponse(true,"deleted");
	}
	
	// Get all file descriptors for a given userID
	public List<FileResponse> GetFilesFor( FileDescriptorRepo fileRepo,  Long userID )
	{
		// Get files
		List<FileDescriptor> files = fileRepo.findAllByUserId(userID);
		List<FileResponse> resFiles = new ArrayList<FileResponse>();
		
		// Hide data
		if(files != null)
		{
			for( FileDescriptor f : files )
				resFiles.add(new FileResponse(f));
		}
		
		// Return
		return resFiles;
	}
	
	/// TEST STUFF
	public void GenerateRandomFileDescriptor( FileDescriptorRepo fileRepo,  Long userID )
	{
		FileDescriptor newDesc = new FileDescriptor();
		newDesc.SetAll(userID, defaultDirectory, "randomFile", ".txt", (long) 512);
		fileRepo.save(newDesc);
	}
}
