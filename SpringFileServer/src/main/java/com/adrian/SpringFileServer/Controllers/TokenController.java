package com.adrian.SpringFileServer.Controllers;

import java.util.ArrayList;
import java.util.Random;

import com.adrian.SpringFileServer.Model.UserToken;

public class TokenController
{
	ArrayList<UserToken> tokens = new ArrayList<UserToken>();
	Random rand = new Random();
	
	public UserToken GetToken( String tokenString )
	{
		if(tokenString == null) return null;
		
		for(int i = 0; i < tokens.size(); i++)
			if( tokens.get(i).getToken().equals(tokenString))
				return tokens.get(i);
		
		return null;
	}
	
	public UserToken GetToken( Long id )
	{
		if(id == null) return null;
		
		for(int i = 0; i < tokens.size(); i++)
			if( tokens.get(i).getUserID().equals(id) )
				return tokens.get(i);
		
		return null;
	}
	
	
	public UserToken GetTokenFor( Long userID )
	{
		// ID ERROR
		if(userID == null) return null;
		
		// Reuse same token
		UserToken existingToken = GetToken(userID);
		if(existingToken != null) return existingToken;
		
		// Try generate token, a couple of times
		for(int i = 0; i < 5; i++)
		{
			Long tokenNumber =  rand.nextLong();
			if(tokenNumber < 0) tokenNumber = -tokenNumber;
			String tokenString = tokenNumber.toString();
			
			existingToken = GetToken( tokenString );
			
			// Add new token
			if(existingToken == null)
			{
				UserToken newToken = new UserToken(userID, tokenString);
				tokens.add(newToken);
				
				return newToken;
			}
		}
		
		// Failed to create token
		return null;
	}
}
