package com.adrian.SpringFileServer.Controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adrian.SpringFileServer.Model.DeleteFileResponse;
import com.adrian.SpringFileServer.Model.FileDescriptor;
import com.adrian.SpringFileServer.Model.FileResponse;
import com.adrian.SpringFileServer.Model.LoginResponse;
import com.adrian.SpringFileServer.Model.NewAccountResponse;
import com.adrian.SpringFileServer.Model.RequestDeleteFile;
import com.adrian.SpringFileServer.Model.RequestDownloadFile;
import com.adrian.SpringFileServer.Model.RequestLogin;
import com.adrian.SpringFileServer.Model.RequestNewAccount;
import com.adrian.SpringFileServer.Model.RequestUpload;
import com.adrian.SpringFileServer.Model.UploadDataResponse;
import com.adrian.SpringFileServer.Model.User;
import com.adrian.SpringFileServer.Repositories.FileDescriptorRepo;
import com.adrian.SpringFileServer.Repositories.UserRepo;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/fileserver")
public class APIEndPoints
{
	@Autowired
	UserRepo userRepo;
	
	@Autowired
	FileDescriptorRepo fileRepo;
	
	// ----------------------------------------- Test Stuff (should be dumped or unmapped) --------------------------------------------------------- //
	
	// List users TODO: ONLY FOR TEST
	/*@GetMapping("/users")
	public List<User> GetAllUsers() { return UsersController.Instance().GetAllUsers(userRepo); }*/
	
	// Download test
	// @GetMapping("/files/downloadtest")
	public byte[] Download(  )
	{
		return new byte[128];
	}
	
	// ----------------------------------------- User access --------------------------------------------------------- //
	
	// Try create account. Return String with creation result
	@PostMapping("/users/newAccount")
	public NewAccountResponse TryCreateUser( @RequestBody RequestNewAccount req )
	{
		System.out.println("REQUEST: NEW ACCOUNT");
		return UsersController.Instance().TryCreateUser(userRepo,req);
	}
	
	// Try login to account. Returns JSON with login result and token, or error message
	@PostMapping("/users/login")
	public LoginResponse TryLogin( @RequestBody RequestLogin req )
	{
		System.out.println("REQUEST: LOGIN");
		return UsersController.Instance().TryLogin(userRepo,req);
	}
	
	// Upload request
	@PostMapping("/users/storageAmount")
	public Long UploadBinary( @RequestBody String reqText )
	{
		System.out.println("REQUEST: FILES");
		
		// Verify token
		Long userID = UsersController.Instance().GetIDByToken( reqText );
		if( userID == null ) return (long) -1;

		// User
		return UsersController.Instance().GetUserMaxStorage( userRepo, userID );
	}
	
	
	// ----------------------------------------- File access --------------------------------------------------------- //
	
	// Try get files, TODO: return actual file description list for Angular
	@PostMapping("/files")
	public List<FileResponse> GetFiles( @RequestBody String reqText )
	{
		System.out.println("REQUEST: FILES");
		
		// Verify token
		Long userID = UsersController.Instance().GetIDByToken( reqText );
		if( userID == null ) return null;
		
		// Get Files
		List<FileResponse> files = FilesController.Instance().GetFilesFor(fileRepo, userID);
		
		// TODO: test for some errors?
		if(files == null) System.out.println("LIST = NULL");
		
		// User
		return files;
	}
	
	
	
	@PostMapping("/files/delete")
	public DeleteFileResponse DeleteFile( @RequestBody RequestDeleteFile req )
	{
		System.out.println("REQUEST: DELETE FILE "+req.getFileID());
		
		// Verify token
		Long userID = UsersController.Instance().GetIDByToken( req.getUserToken() );
		if( userID == null ) return new DeleteFileResponse(false,"unauth");

		// User
		System.out.println("----- Found token -----");
		return FilesController.Instance().DeleteFile(fileRepo, userID, req);
	}
	
	
	@PostMapping("files/download")
	public ResponseEntity<Resource> download(@RequestBody RequestDownloadFile req) throws IOException {

		System.out.println("REQUEST: DOWNLOAD FILE "+req.getFileID());
		
		//return "OK"; /*ResponseEntity.ok().body(null)*/
		
		// Verify token
		Long userID = UsersController.Instance().GetIDByToken( req.getUserToken() );
		if( userID == null ) return ResponseEntity.badRequest().body(null);					// TODO: will this work?

		// Get file from files controller
		File file = FilesController.Instance().DownloadFile(fileRepo, userID, req);
		if(file == null) return ResponseEntity.badRequest().body(null);					// TODO: will this work?
		
		// FIS
		FileInputStream fis = null;
		fis = new FileInputStream(file);
		if(fis == null) return ResponseEntity.badRequest().body(null);					// TODO: will this work?

		// Response
	    InputStreamResource resource = new InputStreamResource(fis);
	    HttpHeaders headers = new HttpHeaders();							// TODO: will this work?
	    
	    System.out.println("UPLOADING FILE");
	    
	    // return ResponseEntity.ok().body(null);
	    
	    return ResponseEntity.ok()
	    		.contentType(MediaType.APPLICATION_OCTET_STREAM)
	    		.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"") 
	    		.body(resource);
	    
	    //return ResponseEntity.ok() .headers(headers) .contentLength(file.length()) .contentType(MediaType.APPLICATION_OCTET_STREAM) .body(resource);
	}

	
	// Upload request
	@PostMapping("/files/upload")
	public int UploadBinary( @RequestBody RequestUpload reqUpload )
	{
		System.out.println("REQUEST: UPLOAD FILE "+reqUpload.getFileName());
		// REQUEST ERROR
		if( reqUpload == null ) return -1;
		
		// Token
		Long userID = UsersController.Instance().GetIDByToken( reqUpload.getUserToken() );
		
		// TOKEN ERROR
		if( userID == null ) return -1;
		
		Long userMaxStorage = UsersController.Instance().GetUserMaxStorage( userRepo, userID );
		
		// Treat request
		return FilesController.Instance().OnReceivedUploadRequest(fileRepo, reqUpload, userID, userMaxStorage);
	}
	
	// Upload Binaries
	@PostMapping("/files/uploadBin/{uploadID}")
	public UploadDataResponse UploadBinary( @RequestBody byte[] reqBytes, @PathVariable int uploadID )
	{
		System.out.println("REQUEST: UPLOAD FILE BYTES, id = "+uploadID);
		
		System.out.println("Starting Upload, ID = " + uploadID);
		
		String res = FilesController.Instance().OnReceivedUpload(fileRepo, reqBytes, uploadID );
		
		System.out.println("UPLOAD RESULT = " + res);
		
		return new UploadDataResponse( res );
	}
}
