package com.adrian.SpringFileServer.Controllers;

import com.adrian.SpringFileServer.Model.LoginResponse;
import com.adrian.SpringFileServer.Model.NewAccountResponse;
import com.adrian.SpringFileServer.Model.RequestLogin;
import com.adrian.SpringFileServer.Model.RequestNewAccount;
import com.adrian.SpringFileServer.Model.User;
import com.adrian.SpringFileServer.Model.UserToken;
import com.adrian.SpringFileServer.Repositories.UserRepo;

import java.util.List;
import java.util.Optional;

public class UsersController
{
	// Singleton
	private static UsersController instance;
	
	public static UsersController Instance()
	{
		if(instance == null) instance = new UsersController();
		return instance;
	}
	
	// Vars
	TokenController tokenController = new TokenController();

	// Resturns all users in the repo
	public List<User> GetAllUsers(UserRepo userRepo)
	{
		return userRepo.findAll();
	}
	
	// Returns UserID of a given token. If user isnt logged in, return null
	public Long GetIDByToken( String token )
	{
		UserToken uToken = tokenController.GetToken(token);
		
		if(uToken == null) return null;
		else return uToken.getUserID();
	}
	
	public Long GetUserMaxStorage( UserRepo userRepo, Long userID )
	{
		Optional<User> foundUser = userRepo.findById(userID);
		
		if(foundUser.isPresent())
		{
			User user = foundUser.get();
			return user.getMaxStorage();
		}

		return (long)-1;
	}
	
	
	// Returns a JSON with the login result, for a given request
	public LoginResponse TryLogin( UserRepo userRepo, RequestLogin req )
	{
		// TEST FOR: errors on request
		if( req == null ) return new LoginResponse( false, "Invalid request!", "" );

		// Search for user
		User foundUser = userRepo.findByLoginAndPassword(req.login, SHA.Instance().SHAField(req.password) );
		
		// Login result
		if(foundUser != null)
		{
			// Token generation
			UserToken uToken = tokenController.GetTokenFor(foundUser.getId());
			if(uToken == null)
			{
				System.out.println("Token generation failed for userID = "+foundUser.getId());
				return new LoginResponse( false, "Server side error!", "" );
			}
			
			// Generate token
			return new LoginResponse( true, uToken.getToken(), foundUser.getLogin() );
		}
		else return new LoginResponse( false, "Invalid credentials!", "" );
	}
	
	// Tries to create a user
	public NewAccountResponse TryCreateUser( UserRepo userRepo, RequestNewAccount req )
	{
		// TEST FOR: errors on request
		if( req == null ) return new NewAccountResponse(false, "Invalid request" );
		if( req.getPassword() == null ) return new NewAccountResponse(false, "Invalid password");
		if( req.getConfirmPass() == null ) return new NewAccountResponse(false, "Invalid password confirmation");
		if( req.getLogin()    == null ) return new NewAccountResponse(false, "Invalid login name");
		
		// Validate fields
		if( !req.getPassword().equals(req.getConfirmPass()) )  return new NewAccountResponse(false, "Passwords don't match");
		if( req.getPassword().length() < 8 )  return new NewAccountResponse(false, "Password too short");
		if( req.getPassword().length() < 8 )  return new NewAccountResponse(false, "Username too short");
		
		// TEST FOR: Existing user
		User existing = userRepo.findByLogin(req.getLogin());
		if(existing != null ) return new NewAccountResponse(false, "Username already in use");
		
		// Create account directly (NO CONFIRMATION)
		String shaPassword = SHA.Instance().SHAField(req.getPassword());
		
		User u = new User();
		u.setLogin(req.login);
		u.setPassword(shaPassword);
		u.setMaxStorage((long)1000000000);
		
		userRepo.save(u);	// TODO: possible error?

		return new NewAccountResponse(true, "");
	}

}
