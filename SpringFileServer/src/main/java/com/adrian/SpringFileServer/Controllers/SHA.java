package com.adrian.SpringFileServer.Controllers;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA
{
	// Singleton
	private static SHA instance;
	
	public static SHA Instance()
	{
		if(instance == null) instance = new SHA();
		return instance;
	}
	
	// Constructor
	public SHA()
	{
		try 
		{
			digest = MessageDigest.getInstance("SHA-256");
		} 
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}
	}

	// SHA
	MessageDigest digest;
	
	public String SHAField(String field)
	{
		byte[] hash = digest.digest(field.getBytes(StandardCharsets.UTF_8));
		String res = "";
		
		for(int i = 0; i < hash.length; i++ ) res += String.format("%02x", hash[i]);
		
		return res;
	}
}
