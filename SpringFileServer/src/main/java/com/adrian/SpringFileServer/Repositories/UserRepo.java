package com.adrian.SpringFileServer.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adrian.SpringFileServer.Model.User;

public interface UserRepo extends JpaRepository<User,Long>
{
	User findByLogin( String login );
	User findByLoginAndPassword( String login, String password );
}
