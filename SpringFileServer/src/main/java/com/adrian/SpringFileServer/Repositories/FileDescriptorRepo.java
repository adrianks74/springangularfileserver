package com.adrian.SpringFileServer.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adrian.SpringFileServer.Model.FileDescriptor;

public interface FileDescriptorRepo  extends JpaRepository<FileDescriptor,Long>
{
	List<FileDescriptor> findAllByUserId( Long userId );
	
	FileDescriptor findByIdAndUserId( Long id, Long userId );
}
