package com.adrian.SpringFileServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.adrian.SpringFileServer.Controllers.FilesController;
import com.adrian.SpringFileServer.Controllers.UsersController;

@SpringBootApplication
public class SpringFileServerApplication
{
	public static void main(String[] args)
	{
		UsersController.Instance();
		FilesController.Instance();
		
		SpringApplication.run(SpringFileServerApplication.class, args);
	}
}
