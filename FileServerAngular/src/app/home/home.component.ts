import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../services/userService';
import { LoggedInfo } from './../model/loggedInfo';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit
{
  loggedInfo:LoggedInfo = null;

  constructor(private router: Router,private loginService:UserService) { }

  ngOnInit(): void
  {
    this.loggedInfo = this.loginService.fetchLoggedInfo();
    if(this.loggedInfo.isLogged() )
    {
      console.log("Logged as "+this.loggedInfo.username);
      console.log("Stored token "+this.loggedInfo.token);

      this.router.navigateByUrl('files');
    }

  }

}
