import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/userService';
import { LoggedInfo } from './../model/loggedInfo';

@Component({
  selector: 'app-basicheader',
  templateUrl: './basicheader.component.html',
  styleUrls: ['./basicheader.component.css']
})
export class BasicheaderComponent implements OnInit
{

  loggedInfo:LoggedInfo = null;


  constructor( private router: Router, private loginService:UserService )
  {
  }

  ngOnInit(): void
  {
    this.loggedInfo = this.loginService.fetchLoggedInfo();
  }

  public isLoggedIn():boolean
  {
    if(this.loggedInfo == null) return false;

    return this.loggedInfo.isLogged();
  }

  public logOff()
  {
    this.loginService.logOff();
    this.router.navigateByUrl('login');
  }

  goToSubscribe()
  {
    this.router.navigateByUrl('signup');
  }

  goToLogin()
  {
    this.router.navigateByUrl('login');
  }

}
