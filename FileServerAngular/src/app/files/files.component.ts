import { DeleteFileResponse } from './../model/deleteFileResponse';
import { FilesService } from './../services/filesService';
import { FileResponse } from './../model/fileResponse';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/userService';
import { LoggedInfo } from './../model/loggedInfo';
import { UploadDataResponse } from '../model/uploadDataResponse';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit
{
  state = "files";

  loggedInfo:LoggedInfo = null;

  files:Array<FileResponse>;
  maxStorage:number = 0;
  curStorage:number = 0;

  error:any;

  downloadingFile:FileResponse = null;

  uploadingFile:File = null;
  uploadProgress:number;

  constructor(private router: Router, private loginService:UserService, private fileService:FilesService) { }

  public logOff()
  {
    // TODO pop up?

    this.loginService.logOff();
    this.router.navigateByUrl('login');
  }

  public goToFiles( ):void
  {
    this.state = "files";
  }

  public sizeString( size:number ):String
  {
    if(size < 1024) return (size/1024).toFixed(3)+" KB";
    else if(size < 1048576 ) return (size/1024).toFixed(1)+" KB";
    else if(size < 1073741824 ) return (size/1048576).toFixed(2)+" MB";
    else return (size/1073741824).toFixed(3)+" GB";
  }

  sortByFileSize():void
  {
    this.files = this.files.sort((a,b) => a.sizeBytes > b.sizeBytes ? -1 : (a.sizeBytes < b.sizeBytes ? 1 : 0));
  }

  maxStorageString():String
  {
    if(this.maxStorage <= 0) return "...";
    else return this.sizeString(this.curStorage)+" / "+ this.sizeString(this.maxStorage)
      +" ("+ (( this.curStorage /this.maxStorage  ) *100).toFixed(2) +"%)";
  }

  /////////////////////////////////////////////////////////////// GET FILES

  ngOnInit(): void
  {
    this.loggedInfo = this.loginService.fetchLoggedInfo();

    // Not logged locally
    if( !this.loggedInfo.isLogged() )
    {
      console.log( "not logged in" );
      this.logOff();
    }
    // Logged, fetch files
    else { this.fecthFiles(); }
  }

  fecthFiles():void
  {
    console.log( "fetch files" );
    this.fileService.getfiles( this.loggedInfo.token ).subscribe
      (
        (data:Array<FileResponse>) => this.onReceivedFiles( data ),
        (err:any) => this.onGetFilesError( err )
      );

      this.maxStorage = 0;

      this.loginService.userMaxStorage( this.loggedInfo.token ).subscribe
      (
        (data:number) => this.onReceivedMaxStorage( data ),
      );

  }

  public onReceivedMaxStorage(data:number):void
  {
    this.maxStorage = data;
  }

  public onReceivedFiles(data:Array<FileResponse>):void
  {
    // Invalid token. Session ended
    if(data == null)
    {
      console.log( "invalid token" );
      this.logOff();
    }
    else
    {
      console.log( "received files" );

      this.files = data;
      this.sortByFileSize();

      this.calcCurStorage();

      console.log( data );
    }
  }

  public calcCurStorage()
  {
    this.curStorage = 0;

    this.files.forEach(element => {
      this.curStorage += element.sizeBytes;
    });
  }



  public onGetFilesError(err:any):void
  {
    this.logOff();
  }


  /////////////////////////////////////////////////////////////// FILE DELETE

  public deleteFile( fileID )
  {
    this.state = "deleting";

    console.log("delete "+fileID);

    this.fileService.deleteFile( this.loggedInfo.token, fileID ).subscribe
      (
        (data:DeleteFileResponse) => this.onDeletedFiles( data ),
        (err:any) => this.onDeleteFileError( err )
      );


  }

  public onDeletedFiles( data:DeleteFileResponse )
  {
    console.log("onDeletedFiles = "+data);

    if(data.success)
    {
      this.fecthFiles();
    }
    else
    {
      if(data.errMessage ==="err")
      {

      }
      else if(data.errMessage ==="unauth")
      {
        this.logOff();
      }
    }

    this.state = "files";
  }

  public onDeleteFileError( err:any )
  {
    // Connection error?
    console.log("onDeleteFileError "+err);
    this.logOff();
  }

  /////////////////////////////////////////////////////////////// FILE DOWNLOAD

  public downloadFile( file:FileResponse )
  {
    console.log("download "+file.fileID);
    this.downloadingFile = file;

    this.fileService.downloadFile( this.loggedInfo.token, file.fileID ).subscribe
      (
        (data:Blob) => this.onDownloadedFile( data )
      );
  }

  public onDownloadedFile( data:Blob )
  {
    console.log("RES = "+data);
    console.log("type = "+data.type);
    console.log("size = "+data.size);

    // Download gambi
    const objUrl = window.URL.createObjectURL(data);

    // Oject URL
    let fileLink = document.createElement('a');
    fileLink.href = objUrl;

    // it forces the name of the downloaded file
    if(this.downloadingFile == null)  fileLink.download = "unknown";
    else fileLink.download = this.downloadingFile.name;

    fileLink.click();
  }

  /////////////////////////////////////////////////////////////// FILE UPLOAD

  sendUploadRequest(file: File)
  {
    console.log("Sending Request upload "+file.name);

    this.uploadingFile = file;

    this.state = "uploading";

    this.fileService.uploadRequest( this.loggedInfo.token, file ).subscribe
      (
        (data:Number) => this.onUploadedRequestSent( data ),
        (err:any) => this.onFileUploadError(err)
      );
  }

  onUploadedRequestSent(data:Number)
  {
    console.log("Request upload sent. ID received = "+data);

    if(data == null || data < 0)
    {
      this.onFileUploadError("Received invalid Upload ID");
      return;
    }

    this.fileService.uploadFileData( data, this.uploadingFile ).subscribe
      (
        (data:UploadDataResponse) => this.onUploadedFileData( data ),
        (err:any) => this.onFileUploadError(err)
      );
  }

  onUploadedFileData(data:UploadDataResponse)
  {
    console.log("Upload res = "+data);

    // Sucess
    if(data.res === "ok")
    {
      this.state = "files";
      this.fecthFiles();
    }
    // ERRO
    else
    {
      console.log("Didnt receive OK on upload!");
      this.onFileUploadError(data);
    }
  }

  onFileUploadError(err:any)
  {
    this.state = "uploadFailed";
    this.uploadingFile = null;
    console.log("Upload error = "+err);
  }

  fileChange(event)
  {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) this.sendUploadRequest(fileList[0]);
  }

  public startUpload()
  {
    console.log("star upload ");
  }
}
