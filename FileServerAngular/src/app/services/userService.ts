import { CookieService } from 'ngx-cookie-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup } from '@angular/forms';
import { LoggedInfo } from '../model/loggedInfo';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor( private http:HttpClient, private cookieService:CookieService ) { }



  public login( form:FormGroup ):Observable<any>
  {
    let headers = new HttpHeaders ( { 'Content-Type': 'application/json'} );

    return this.http.post('http://localhost:8080/fileserver/users/login', JSON.stringify(form.value), { headers: headers } );
  }

  public newaccount( form:FormGroup ):Observable<any>
  {
    let headers = new HttpHeaders ( { 'Content-Type': 'application/json'} );

    return this.http.post('http://localhost:8080/fileserver/users/newAccount', JSON.stringify(form.value), { headers: headers } );
  }

  public userMaxStorage( token:String ):Observable<any>
  {
    let headers = new HttpHeaders ( { 'Content-Type': 'text/plain'} );

    return this.http.post('http://localhost:8080/fileserver/users/storageAmount', token, { headers: headers } );
  }

  public setLoginToken( li:LoggedInfo ): void
  {
    this.cookieService.set('loggedToken', li.token );
    this.cookieService.set('loggedAs', li.username );

    console.log("Logged as "+li.username);
  }

  public fetchLoggedInfo( ): LoggedInfo
  {
    let li:LoggedInfo = new LoggedInfo();
    li.token = this.cookieService.get( 'loggedToken' );
    li.username = this.cookieService.get( 'loggedAs' );

    return li;
  }

  public logOff() : void
  {
    this.setLoginToken( <LoggedInfo>({ username:"", token:"" }) );
  }
}
