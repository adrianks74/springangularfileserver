import { UploadRequest } from './../model/uploadRequest';
import { RequestDeleteFile } from './../model/requestDeleteFile';
import { RequestDownloadFile } from './../model/requestDownloadFile';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class FilesService
{
  constructor( private http:HttpClient ) { }

  public getfiles( token:String ):Observable<any>
  {
    let headers = new HttpHeaders ( { 'Content-Type': 'application/json'} );
    return this.http.post('http://localhost:8080/fileserver/files', token, { headers: headers } );
  }

  public deleteFile( token:String, fileID ):Observable<any>
  {
    let headers = new HttpHeaders ( { 'Content-Type': 'application/json'} );
    return this.http.post('http://localhost:8080/fileserver/files/delete', <RequestDeleteFile>({ fileID:fileID, userToken:token }), { headers: headers } );
  }

  public downloadFile( token:String, fileID ):Observable<any>
  {
    let headers = new HttpHeaders ( { 'Content-Type': 'application/json'} );
    return this.http.post('http://localhost:8080/fileserver/files/download',
      <RequestDownloadFile>({ fileID:fileID, userToken:token }), { headers: headers, responseType: 'blob' }, );
  }

  public uploadRequest( token:String, file:File )
  {
    let ur:UploadRequest = <UploadRequest>({
      userToken:token,
      fileSizeBytes:file.size,
      fileName:file.name
    })

    let headers = new HttpHeaders ( { 'Content-Type': 'application/json'} );

    return this.http.post('http://localhost:8080/fileserver/files/upload', ur, { headers: headers } );
  }

  public uploadFileData( uploadID:Number, file:File )
  {
    /*let formData = new FormData();
    formData.append("",file);*/

    let headers = new HttpHeaders ( { 'Content-Type': 'application/octet-stream' } );

    return this.http.post('http://localhost:8080/fileserver/files/uploadBin/'+uploadID, file, { headers: headers , reportProgress:true} );
  }
}
