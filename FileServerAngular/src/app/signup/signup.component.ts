import { NewAccountResponse } from './../model/newAccountResponse';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UserService } from './../services/userService';
import { Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit
{
  form:FormGroup;
  state = "idle";
  generalError = null;

  constructor( private router: Router,private http:HttpClient, private loginService:UserService )
  {
  }

  onSubmit(): void
  {
    console.log(this.form);

    this.loginService.newaccount(this.form).subscribe
    (
      (data:NewAccountResponse) => this.onCreateAccountResponse( data ),
      (err:any) => this.onCreateAccountError( err )
    );

    this.state ="creating";
    this.generalError = "null";
  }

  onCreateAccountResponse( data:NewAccountResponse ) : void
  {
    console.log("RESPONSE");
    console.log(data);
    console.log(data.created);
    console.log(data.errMessage);

    if( data.created )
    {
      this.state = "created";
      this.router.navigateByUrl("home");
    }
    else
    {
      this.generalError = data.errMessage;
      this.state = "idle";
    }
  }

  goToLogin()
  {
    this.router.navigateByUrl("login");
  }

  onCreateAccountError( data ) : void
  {
    console.log("ERROR");
    console.log(data);

    this.generalError = "No server response";
    this.state = "idle";
  }

  ngOnInit(): void
  {
    this.form = new FormGroup(
      {
        login: new FormControl(null),
        password: new FormControl(null),
        confirmPass: new FormControl(null)
      }
    );
  }
}
