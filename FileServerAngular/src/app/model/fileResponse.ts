export class FileResponse
{
  public name:string;
  public sizeBytes;
  public fileID;

  public GetSize()
  {
    if(this.sizeBytes < 1024) return this.sizeBytes+" bytes";
    else if(this.sizeBytes < 1048576 ) return (this.sizeBytes/1024)+" KB";
    else if(this.sizeBytes < 1073741824 ) return (this.sizeBytes/1048576)+" MB";
    else return (this.sizeBytes/1073741824)+" GB";
  }

}
