export class LoginResponse
{
  public success:boolean;
  public token:string;
  public username:string;
}
