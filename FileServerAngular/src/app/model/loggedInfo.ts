export class LoggedInfo
{
  public username:string;
  public token:string;


  public isLogged():boolean
  {
    if( this.token == null ) return false;
    if( this.token.length == 0 ) return false;

    return true;
  }
}
