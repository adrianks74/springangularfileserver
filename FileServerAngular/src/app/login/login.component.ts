import { LoggedInfo } from './../model/loggedInfo';
import { UserService } from './../services/userService';
import { LoginResponse } from './../model/loginResponse';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit
{
  form:FormGroup;
  state = "idle";
  generalError = null;

  constructor(private router: Router, private http:HttpClient, private loginService:UserService)
  {

  }

  onSubmit(): void
  {
    // TODO: validate fields
    console.log(this.form);

    this.loginService.login(this.form).subscribe
    (
      (data:LoginResponse) => this.onLoginResponse( data ),
      (err:any) => this.onLoginError( err )
    );

    this.state ="logging";
    this.generalError = "null";
  }

  onLoginResponse( data:LoginResponse ) : void
  {
    console.log("Login RESPONSE");
    console.log(data);
    console.log(data.success);
    console.log(data.token);
    console.log(data.username);

    if( data.success )
    {
      this.loginService.setLoginToken( <LoggedInfo>({ username: data.username, token: data.token }) );
      this.router.navigateByUrl("files");
    }
    else
    {
      this.generalError = data.token;
      this.state = "idle";
    }
  }

  onLoginError( err:any ) : void
  {
    let loginRes:LoginResponse = err;

    console.log("Login ERROR");
    console.log(err);

    // TODO: display error
    this.state = "idle";
    this.generalError = "No server response";
  }

  ngOnInit(): void
  {
    this.form = new FormGroup(
      {
        login: new FormControl(null),
        password: new FormControl(null)
      }
    );
  }

}
