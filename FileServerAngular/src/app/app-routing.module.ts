import { FilesComponent } from './files/files.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path:"", redirectTo:"/home", pathMatch: "full"},
  { path:"home", component: HomeComponent },
  { path:"signup", component: SignupComponent },
  { path:"login", component: LoginComponent },
  { path:"files", component: FilesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
